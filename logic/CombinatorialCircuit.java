package logic;
import java.util.Set;
import java.util.TreeSet;

public class CombinatorialCircuit {
	
	Set<LogicVariable> logicVars = new TreeSet<LogicVariable>();  
	
	public boolean addVariable(LogicVariable a) {

		return logicVars.add(a);
	}

	public LogicVariable getVariableByName(String var) {

		for (LogicVariable lv : logicVars) {
			if (lv.getName().equals(var))
				return lv;
		}
		
		return null;
	}

}
