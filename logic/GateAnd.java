package logic;

public class GateAnd extends LogicGate {

	public GateAnd(LogicVariable output, LogicVariable input1, LogicVariable input2) throws ColisionException, CycleException {
		super(output, input1, input2, "AND");
		
		output.setValue(input1.getValue() && input2.getValue());
	}
	
	public boolean logic() {
		return input1.getValue() && input2.getValue();
	}

}
