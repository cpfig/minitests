package logic;

public class GateNot extends LogicGate {

	public GateNot(LogicVariable output, LogicVariable input) throws ColisionException, CycleException {
		super(output, input, "NOT");
		
		output.setValue(!input.getValue());
	}
	
	@Override
	public LogicVariable[] getInputs() {
		return new LogicVariable[]{input1};
	}
	
	public boolean logic() {
		return !input1.getValue();
	}

}
