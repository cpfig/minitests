package logic;

public class GateOr extends LogicGate {

	public GateOr(LogicVariable output, LogicVariable input1, LogicVariable input2) throws ColisionException, CycleException {
		super(output, input1, input2, "OR");
		
		output.setValue(input1.getValue() || input2.getValue());
	}
	
	public boolean logic() {
		return input1.getValue() || input2.getValue();
	}

}
