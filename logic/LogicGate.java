package logic;

public abstract class LogicGate {
	
	LogicVariable output, input1, input2;
	String symbol;

	public LogicGate (LogicVariable output, LogicVariable input1, LogicVariable input2, String symbol) throws ColisionException, CycleException {
		this.output = output;
		this.input1 = input1;
		this.input2 = input2;
		this.symbol = symbol;
		
		if (output.getCalculatedBy() == null)
			output.setCalculatedBy(this);
		else throw new ColisionException();
		
		output.setFormula(symbol + "(" + input1.getFormula() + "," + input2.getFormula() + ")");
	}
	
	public LogicGate (LogicVariable output, LogicVariable input, String symbol) throws ColisionException, CycleException {
		this.output = output;
		this.input1 = input;
		this.input2 = null;
		this.symbol = symbol;
		
		if (output.equals(input1.calculatedBy.getInputs()[0]) || 
				output.equals(input1.calculatedBy.getInputs()[1]))
			throw new CycleException();
		
		if (output.equals(this.getInputs()[0]))
			throw new CycleException();
		
		if (output.getCalculatedBy() == null)
			output.setCalculatedBy(this);
		else throw new ColisionException();
		
		output.setFormula(symbol + "(" + input1.getFormula() + ")");

	}
	
	public LogicVariable getOutput() {
		return output;
	}
	
	public LogicVariable[] getInputs() {
		return new LogicVariable[]{input1, input2};
	}
	
	public String getSymbol() {
		return symbol;
	}

	public String getFormula() {
		return output.getFormula();
	}

	protected abstract boolean logic();
	
}
