package logic;

public class LogicVariable implements Comparable<LogicVariable>{
	
	String name;
	boolean value;
	LogicGate calculatedBy;
	String formula;

	public LogicVariable(String name, boolean value) {
		this.name = name;
		this.value = value;
		this.formula = name;
	}

	public LogicVariable(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
	
	public boolean getValue() {
		if (calculatedBy == null)
			return value;
		else return calculatedBy.logic();
	}

	public void setValue(boolean b) {
		this.value = b;
	}
	
	@Override
	public boolean equals(Object o) {
		LogicVariable other = (LogicVariable) o;
		return this.getName().equals(other.getName());
	}

	public LogicGate getCalculatedBy() {
		return calculatedBy;
	}
	
	public void setCalculatedBy(LogicGate gate) {
		this.calculatedBy = gate;
	}

	public String getFormula() {
		return formula;
	}
	
	public void setFormula(String form) {
		this.formula = form;
	}

	@Override
	public int compareTo(LogicVariable lv) {
		return this.getName().compareTo(lv.getName());
	}

}
