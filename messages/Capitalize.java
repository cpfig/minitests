package messages;

public class Capitalize implements MessageFormatter {

	@Override
	public String formatMessage(String text) {
		String[] words = text.split(" ");
		String inCaps = "";
		
		for (String wd : words) {
			char first = wd.charAt(0);
			inCaps += Character.toUpperCase(first);
			inCaps += wd.substring(1);
			inCaps += " ";
		}
		
		
		return inCaps.trim();
	}

}
