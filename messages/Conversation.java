package messages;
import java.util.ArrayList;
import java.util.TreeSet;

public class Conversation {
	
	int maxUsers;
	TreeSet<String> users;
	ArrayList<IdentifiedMessage> msgs;
	
	public Conversation(int maxUsers) {
		this.maxUsers = maxUsers;
		this.users = new TreeSet<String>();
		this.msgs = new ArrayList<IdentifiedMessage>();
	}

	public Object getMaxNumberOfUsers() {
		return maxUsers;
	}

	public Object getUserCount() {
		return users.size();
	}

	public void addUser(String user) {
		if (users.size() < maxUsers)
			users.add(user);
	}

	public void addMessage(IdentifiedMessage im) {
		if (users.contains(im.getUser()))
			msgs.add(im);
	}

	public Object getMessageCount() {
		return msgs.size();
	}

	public void removeUser(String user) {
		if (users.contains(user))
			users.remove(user);
	}

	public ArrayList<Message> getUserMessages(String user) {
		ArrayList<Message> userMessages = new ArrayList<Message>();
		
		if (users.contains(user)) {
			for  (IdentifiedMessage im : msgs) {
				if (im.getUser().equals(user))
					userMessages.add(new Message(im.getText()));
			}
		}
		return userMessages;
	}

}
