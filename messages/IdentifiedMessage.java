package messages;

public class IdentifiedMessage extends Message {

	String user;
	
	public IdentifiedMessage(String user, String text) {
		super(text);
		this.user = user;
	}

	public void setUser(String user) {
		this.user = user;		
	}

	public String getUser() {
		return user;
	}
	
	public String getText() {
		return super.getMessage();
	}
	
	@Override
	public String getMessage() {
		return "[" + user + "]: " + super.getMessage();
	}
	
	@Override
	public boolean equals(Object o) {
		IdentifiedMessage im = (IdentifiedMessage) o;
		return this.getMessage().equals(im.getMessage());
	}
	
	@Override
	public String toString() {
		return "user:[" + getUser() + "], msg:[" + super.getMessage() + "]";
	}
}
