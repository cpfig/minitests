package messages;

public class Message implements Comparable<Message>{
	
	String text;
	MessageFormatter mf;

	public Message(String s) {
		this.text = s;
		this.mf = new Post();
	}

	public Message(Message m) {
		this.text = m.getMessage();
		this.mf = m.getFormatingStrategy();
	}

	public void setMessage(String s) {
		this.text = s;
	}

	public String getMessage() {
		return mf.formatMessage(text);
	}

	public void setFormattingStrategy(MessageFormatter mf) {
		this.mf = mf;
	}
	
	public MessageFormatter getFormatingStrategy() {
		return mf;
	}
	
	@Override
	public boolean equals(Object o) {
		Message m = (Message) o;
		return this.getMessage().equals(m.getMessage());
	}
	
	@Override
	public String toString() {
		return "msg:[" + getMessage() + "]";
		
	}

	@Override
	public int compareTo(Message other) {
		return getMessage().compareTo(other.getMessage());
	}

}
