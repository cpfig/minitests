package messages;

public interface MessageFormatter {
	
	String formatMessage (String text);

}
