package messages;

public class Shout implements MessageFormatter {

	@Override
	public String formatMessage(String text) {
		return text.toUpperCase();
	}

}
